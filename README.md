# Corail - Presentation website

[![build status](https://gitlab.com/corail/corail.gitlab.io/badges/master/build.svg)](https://gitlab.com/corail/corail.gitlab.io/commits/master)

> Presentation website for Corail application: https://corail.gitlab.io.

> The features of the application, the context as well as some details on how to run it can be found at this address.

The actual application is available in the following repository: https://gitlab.com/corail/corail
