import Vue from 'vue'

import Application from './Application'

new Vue({
	el:		'#application',
	render:	h => h(Application)
})
